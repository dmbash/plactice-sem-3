import java.io.File;
import java.io.FileInputStream;

public class FileInput implements InputInterface {
	private String path;
	
	public FileInput(String _path) {
		path = _path;
	}
	
	@Override
	public String getInput() {
		String input = "";
		if (path.length() == 0 ) {
			try {
				throw new Exception("File path is 0-length string");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} 
		
		File file = new File(path);
		FileInputStream fileInputStream = null;
		
		try {
			fileInputStream = new FileInputStream(file);
			
			int _byte;
			
			while ((_byte = fileInputStream.read()) != -1) {
				input += (char) _byte;
			}
			
		} catch (Exception e) {
			System.out.println("Error while reading file");
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
		return input;
	}

}
